/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 12:57:32 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/25 14:18:29 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include <typedef_fractol.h>

struct			s_coords
{
	float		x;
	float		y;
	float		z;
	float		w;
};

struct			s_dcoords
{
	double		x;
	double		y;
};

struct			s_complexe
{
	double		z_r;
	double		z_i;
	double		c_r;
	double		c_i;
};

struct			s_fractdata
{
	double		x_1;
	double		x_2;
	double		y_1;
	double		y_2;
	t_complexe	*c;
	float		zoom;
	int			ite;
};

struct			s_image
{
	char		*cimg;
	void		*img;
	int			bpp;
	int			sizeline;
	int			endian;
	int			width;
	int			height;
};

struct			s_env
{
	t_image			*image;
	t_fractdata		*fract;
	void			*mlx;
	void			*win;
	t_coords		win_size;
	float			zoom_const;
	int				nb_fract;
	t_frac_method	tab_fract[2];
	t_is_in			tab_is_in_fract[5];
};

/*
**color
*/

unsigned int	create_color(int a, int r, int g, int b);

/*
**fractal
*/

t_fractdata		*get_fract_data(float a, float b, float c, float d);
void			modif_fract_data(t_fractdata *fract, t_dcoords pt_to_zoom,
				float zoom_const);
void			modif_pos_point(t_dcoords pt, t_env *env, int x, int y);
void			center_point(t_env *env, t_dcoords pt);
void			draw_julia(t_fractdata *fract, int image_x, int image_y,
				t_env *env);
void			draw_mandel(t_fractdata *fract, int image_x, int image_y,
				t_env *env);
void			draw_burning(t_fractdata *fract, int image_x, int image_y,
				t_env *env);

/*
**is_in_fract
*/

int				is_in_fract(t_fractdata *fract);
int				is_in_fract_3(t_fractdata *fract);
int				is_in_fract_4(t_fractdata *fract);
int				is_in_fract_burning(t_fractdata *fract);
int				is_in_fract_tricorn(t_fractdata *fract);

/*
**set_data
*/

void			fill_fract_tab(t_env *env);
void			fill_fract_data(t_env *env);
int				which_fract(char *av);

/*
**image
*/

t_image			*create_image(void *mlx, int width, int height);
void			image_put_pixel(t_env env, int x, int y,
				unsigned int color);
void			clear_image(t_image *image);

/*
**constructor
*/

int				constructor(t_env *env, char *fname);
t_env			*constructor_env(int win_x, int win_y);

/*
**draw_point
*/

void			draw_x(int x, int y, t_env *env, t_coords pt_a);
void			draw_y(int x, int y, t_env *env, t_coords pt_a);
int				draw(t_env *env);

/*
**press_key
*/

int				press_key(int key_code, t_env *env);
int				mouse(int button, int x, int y, t_env *env);
int				mouse_motion(int x, int y, t_env *env);

#endif
