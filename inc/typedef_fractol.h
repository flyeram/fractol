/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   typedef_fractol.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 18:15:00 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/22 18:17:51 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TYPEDEF_FRACTOL_H
# define TYPEDEF_FRACTOL_H

typedef struct s_env		t_env;
typedef struct s_image		t_image;
typedef struct s_fractdata	t_fractdata;
typedef struct s_dcoords	t_dcoords;
typedef struct s_coords		t_coords;
typedef struct s_complexe	t_complexe;
typedef void				(*t_frac_method)(t_fractdata *, int, int, t_env *);
typedef int					(*t_is_in)(t_fractdata *);

#endif
