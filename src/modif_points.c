/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   modif_points.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 16:13:57 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/25 13:35:26 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

void		modif_pos_point(t_dcoords pt, t_env *env, int x, int y)
{
	t_dcoords	pt_2;

	pt_2.x = pt.x - (x / env->fract->zoom + env->fract->x_1);
	pt_2.y = pt.y - (y / env->fract->zoom + env->fract->y_1);
	env->fract->x_1 += pt_2.x;
	env->fract->x_2 += pt_2.x;
	env->fract->y_1 += pt_2.y;
	env->fract->y_2 += pt_2.y;
}

void		modif_fract_data(t_fractdata *fract,
			t_dcoords pt_zoom, float zoom_const)
{
	fract->x_1 = pt_zoom.x - 1;
	fract->x_2 = pt_zoom.x + 1;
	fract->y_1 = pt_zoom.y - 1;
	fract->y_2 = pt_zoom.y + 1;
	fract->zoom = 400 * zoom_const;
}

void		center_point(t_env *env, t_dcoords pt)
{
	t_dcoords	pt_center;
	t_dcoords	diff;

	pt_center.x = env->win_size.x / 2 / env->fract->zoom + env->fract->x_1;
	pt_center.y = env->win_size.y / 2 / env->fract->zoom + env->fract->y_1;
	diff.x = pt.x - pt_center.x;
	diff.y = pt.y - pt_center.y;
	env->fract->x_1 += diff.x;
	env->fract->x_2 += diff.x;
	env->fract->y_1 += diff.y;
	env->fract->y_2 += diff.y;
}
