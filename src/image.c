/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/23 11:02:21 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/14 12:18:08 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <fractol.h>
#include <stdlib.h>
#include <libft.h>

void		clear_image(t_image *image)
{
	int		off_y;

	off_y = (*image).width * (*image).height * 4 - 1;
	(*image).cimg = ft_memset((*image).cimg, 0, off_y);
}

t_image		*create_image(void *mlx, int width, int height)
{
	t_image		*image;

	if (!(image = (t_image *)malloc(sizeof(t_image))))
		return (NULL);
	if (!((*image).img = mlx_new_image(mlx, width, height)))
		return (NULL);
	(*image).width = width;
	(*image).height = height;
	(*image).bpp = 0;
	(*image).sizeline = 0;
	(*image).endian = 0;
	(*image).cimg = mlx_get_data_addr((*image).img, &((*image).bpp),
					&((*image).sizeline), &((*image).endian));
	return (image);
}

void		image_put_pixel(t_env env, int x, int y, unsigned int color)
{
	int		off_x;
	int		off_y;

	if (x >= 0 && x < env.win_size.x && y >= 0 && y < env.win_size.y)
	{
		off_x = x * (env.image->bpp / 8);
		off_y = (y * env.image->sizeline);
		*((unsigned int *)(env.image->cimg + off_y + off_x)) = color;
	}
}
