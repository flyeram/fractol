/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_in_fract.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 15:52:08 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/25 14:06:22 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <math.h>
#include <libft.h>
#include <mlx.h>
#include <stdlib.h>

int		is_in_fract_burning(t_fractdata *fract)
{
	double	tmp;
	int		i;

	i = 0;
	while ((fract->c->z_r * fract->c->z_r + fract->c->z_i * fract->c->z_i) < 4
			&& i < fract->ite)
	{
		tmp = fract->c->z_r;
		fract->c->z_r = fabs((fract->c->z_r * fract->c->z_r) -
		(fract->c->z_i * fract->c->z_i) + fract->c->c_r);
		fract->c->z_i = fabs((2 * tmp * fract->c->z_i) + fract->c->c_i);
		i++;
	}
	return (i);
}

int		is_in_fract(t_fractdata *fract)
{
	double	tmp;
	int		i;

	i = 0;
	while ((fract->c->z_r * fract->c->z_r + fract->c->z_i * fract->c->z_i) < 4
			&& i < fract->ite)
	{
		tmp = fract->c->z_r;
		fract->c->z_r = (fract->c->z_r * fract->c->z_r) -
		(fract->c->z_i * fract->c->z_i) + fract->c->c_r;
		fract->c->z_i = (2 * tmp * fract->c->z_i) + fract->c->c_i;
		i++;
	}
	return (i);
}

int		is_in_fract_4(t_fractdata *fract)
{
	double	tmp;
	int		i;

	i = 0;
	while ((fract->c->z_r * fract->c->z_r + fract->c->z_i * fract->c->z_i) < 4
			&& i < fract->ite)
	{
		tmp = fract->c->z_r;
		fract->c->z_r = (fract->c->z_r * fract->c->z_r *
		fract->c->z_r * fract->c->z_r) -
		(6 * fract->c->z_r * fract->c->z_r * fract->c->z_i * fract->c->z_i) +
		(fract->c->z_i * fract->c->z_i * fract->c->z_i * fract->c->z_i)
		+ fract->c->c_r;
		fract->c->z_i = (4 * tmp * tmp * tmp * fract->c->z_i) -
		(4 * tmp * fract->c->z_i * fract->c->z_i * fract->c->z_i)
		+ fract->c->c_i;
		i++;
	}
	return (i);
}

int		is_in_fract_3(t_fractdata *fract)
{
	double	tmp;
	int		i;

	i = 0;
	while ((fract->c->z_r * fract->c->z_r + fract->c->z_i * fract->c->z_i) < 4
			&& i < fract->ite)
	{
		tmp = fract->c->z_r;
		fract->c->z_r = (fract->c->z_r * fract->c->z_r * fract->c->z_r) -
		(3 * fract->c->z_r * fract->c->z_i * fract->c->z_i) + fract->c->c_r;
		fract->c->z_i = -(fract->c->z_i * fract->c->z_i * fract->c->z_i) +
		(3 * tmp * tmp * fract->c->z_i) + fract->c->c_i;
		i++;
	}
	return (i);
}

int		is_in_fract_tricorn(t_fractdata *fract)
{
	double	tmp;
	int		i;

	i = 0;
	while ((fract->c->z_r * fract->c->z_r + fract->c->z_i * fract->c->z_i) < 4
			&& i < fract->ite)
	{
		tmp = fract->c->z_r;
		fract->c->z_r = (fract->c->z_r * fract->c->z_r) -
		(fract->c->z_i * fract->c->z_i) + fract->c->c_r;
		fract->c->z_i = (-2 * tmp * fract->c->z_i) + fract->c->c_i;
		i++;
	}
	return (i);
}
