/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 11:15:33 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/25 14:10:22 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <mlx.h>
#include <libft.h>
#include <stdlib.h>

int			mouse(int button, int x, int y, t_env *env)
{
	t_dcoords	pt;

	pt.x = (x / env->fract->zoom + env->fract->x_1);
	pt.y = (y / env->fract->zoom + env->fract->y_1);
	if (button == 6)
	{
		env->zoom_const *= 1.1;
		modif_fract_data(env->fract, pt, env->zoom_const);
		modif_pos_point(pt, env, x, y);
		draw(env);
	}
	if (button == 7 && env->zoom_const > 1)
	{
		env->zoom_const *= 0.9;
		modif_fract_data(env->fract, pt, env->zoom_const);
		modif_pos_point(pt, env, x, y);
		draw(env);
	}
	if (button == 1)
	{
		center_point(env, pt);
		draw(env);
	}
	return (0);
}

int			mouse_motion(int x, int y, t_env *env)
{
	if (env->nb_fract >= 5 && env->nb_fract <= 7)
	{
		env->fract->c->c_r = x / env->fract->zoom + env->fract->x_1;
		env->fract->c->c_i = y / env->fract->zoom + env->fract->y_1;
		draw(env);
	}
	return (0);
}

void		swap_fract(int key_code, t_env *env)
{
	if (key_code == 49)
		env->zoom_const = 1;
	if (key_code == 18)
		env->nb_fract = 0;
	if (key_code == 19)
		env->nb_fract = 1;
	if (key_code == 20)
		env->nb_fract = 2;
	if (key_code == 21)
		env->nb_fract = 3;
	if (key_code == 23)
		env->nb_fract = 4;
	if (key_code == 22)
		env->nb_fract = 5;
	if (key_code == 26)
		env->nb_fract = 6;
	if (key_code == 28)
		env->nb_fract = 7;
	if (key_code == 49 || (key_code >= 18 && key_code <= 23) || key_code == 26
		|| key_code == 28)
		fill_fract_data(env);
	if (key_code == 69)
		env->fract->ite += 10;
	if (key_code == 78 && env->fract->ite > 10)
		env->fract->ite -= 10;
}

int			press_key(int key_code, t_env *env)
{
	if (key_code == 53)
		exit(3);
	if (key_code == 123)
	{
		env->fract->x_1 += 10 / env->fract->zoom;
		env->fract->x_2 -= 10 / env->fract->zoom;
	}
	if (key_code == 124)
	{
		env->fract->x_1 -= 10 / env->fract->zoom;
		env->fract->x_2 += 10 / env->fract->zoom;
	}
	if (key_code == 126)
	{
		env->fract->y_1 += 10 / env->fract->zoom;
		env->fract->y_2 -= 10 / env->fract->zoom;
	}
	if (key_code == 125)
	{
		env->fract->y_1 -= 10 / env->fract->zoom;
		env->fract->y_2 += 10 / env->fract->zoom;
	}
	swap_fract(key_code, env);
	draw(env);
	return (0);
}
