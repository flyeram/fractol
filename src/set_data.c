/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_data.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 16:16:10 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/25 14:15:56 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <libft.h>

void		fill_fract_tab(t_env *env)
{
	env->tab_fract[0] = &draw_mandel;
	env->tab_fract[1] = &draw_julia;
	env->tab_is_in_fract[0] = &is_in_fract_burning;
	env->tab_is_in_fract[1] = &is_in_fract;
	env->tab_is_in_fract[2] = &is_in_fract_3;
	env->tab_is_in_fract[3] = &is_in_fract_4;
	env->tab_is_in_fract[4] = &is_in_fract_tricorn;
}

void		fill_fract_data(t_env *env)
{
	if (env->nb_fract == 1)
		env->fract = get_fract_data(-2.1, 0.6, -1.2, 1.2);
	else if (env->nb_fract >= 5 && env->nb_fract <= 7)
		env->fract = get_fract_data(-1.3, 0.7, -1.2, 1.2);
	else if (env->nb_fract == 3)
		env->fract = get_fract_data(-1.3, 0.7, -1.2, 1.2);
	else if (env->nb_fract == 0)
		env->fract = get_fract_data(-1.85, 0.15, -1.6, 0.8);
	else if (env->nb_fract == 2)
		env->fract = get_fract_data(-1.5, 1.2, -1.2, 1.2);
	else if (env->nb_fract == 4)
		env->fract = get_fract_data(-1.5, 1.2, -1.2, 1.2);
}

int			which_fract(char *av)
{
	if (!(ft_strcmp(av, "mandelbrot")))
		return (1);
	if (!(ft_strcmp(av, "mandelbrot_3")))
		return (2);
	if (!(ft_strcmp(av, "mandelbrot_4")))
		return (3);
	if (!(ft_strcmp(av, "julia")))
		return (5);
	if (!(ft_strcmp(av, "julia_3")))
		return (6);
	if (!(ft_strcmp(av, "julia_4")))
		return (7);
	if (!(ft_strcmp(av, "burning")))
		return (0);
	if (!(ft_strcmp(av, "tricorn")))
		return (4);
	return (-1);
}
