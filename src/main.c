/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:46:20 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/25 14:28:39 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <libft.h>
#include <unistd.h>
#include <fractol.h>
#include <stdlib.h>

void		print_error(void)
{
	ft_putstr("ERROR\n");
	ft_putstr("You have to add the name of the fractal (only one) after");
	ft_putstr(" the name of the progamme");
	ft_putstr(" like : ./fractol julia\n");
	ft_putstr("fractals available :\n");
	ft_putstr("-burning\n");
	ft_putstr("-mandelbrot\n");
	ft_putstr("-mandelbrot_3\n");
	ft_putstr("-mandelbrot_4\n");
	ft_putstr("-tricorn\n");
	ft_putstr("-julia\n");
	ft_putstr("-julia_3\n");
	ft_putstr("-julia_4\n");
}

void		executor(t_env *env)
{
	env->zoom_const = 1;
	mlx_expose_hook((*env).win, draw, env);
	mlx_hook((*env).win, 2, (1L << 0), press_key, env);
	mlx_hook((*env).win, 6, (1L << 6), mouse_motion, env);
	mlx_mouse_hook((*env).win, mouse, env);
	mlx_loop((*env).mlx);
}

int			main(int ac, char **av)
{
	t_env	*env;
	int		win_x;
	int		win_y;
	int		fract_number;

	if (ac != 2)
	{
		print_error();
		return (0);
	}
	win_x = 1079;
	win_y = 960;
	fract_number = which_fract(av[1]);
	if (fract_number == -1)
	{
		print_error();
		return (0);
	}
	env = constructor_env(win_x, win_y);
	env->nb_fract = fract_number;
	fill_fract_tab(env);
	fill_fract_data(env);
	executor(env);
	return (0);
}
