/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_fract.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 11:30:34 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/25 14:17:29 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <libft.h>
#include <mlx.h>
#include <stdlib.h>

t_fractdata	*get_fract_data(float a, float b, float c, float d)
{
	t_fractdata	*new_elem;
	t_complexe	*new_c;

	if (!(new_elem = (t_fractdata *)malloc(sizeof(t_fractdata))))
		return (NULL);
	if (!(new_c = (t_complexe *)malloc(sizeof(t_complexe))))
		return (NULL);
	new_elem->x_1 = a;
	new_elem->x_2 = b;
	new_elem->y_1 = c;
	new_elem->y_2 = d;
	new_elem->zoom = 400;
	new_elem->ite = 100;
	new_c->z_r = 0;
	new_c->z_i = 0;
	new_c->c_r = 0.285;
	new_c->c_i = 0.01;
	new_elem->c = new_c;
	return (new_elem);
}

int			draw(t_env *env)
{
	float		image_x;
	float		image_y;

	clear_image(env->image);
	image_x = 1079;
	image_y = 960;
	if (env->nb_fract >= 0 && env->nb_fract <= 4)
		env->tab_fract[0](env->fract, image_x, image_y, env);
	else
		env->tab_fract[1](env->fract, image_x, image_y, env);
	mlx_put_image_to_window(env->mlx, env->win, env->image->img, 0, 0);
	return (0);
}
