/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fract_algo.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 15:47:10 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/25 14:13:30 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>
#include <libft.h>
#include <mlx.h>
#include <stdlib.h>

void	draw_julia(t_fractdata *fract, int image_x, int image_y, t_env *env)
{
	t_coords	coords;
	int			i;

	coords.x = 0;
	while (coords.x < image_x)
	{
		coords.y = 0;
		while (coords.y < image_y)
		{
			fract->c->z_r = coords.x / fract->zoom + fract->x_1;
			fract->c->z_i = coords.y / fract->zoom + fract->y_1;
			i = env->tab_is_in_fract[env->nb_fract - 4](fract);
			if (i == fract->ite)
				image_put_pixel(*env, coords.x, coords.y, 0x0000FF);
			else
				image_put_pixel(*env, coords.x, coords.y,
				create_color(0, i * 255 / fract->ite, 0,
				(i <= fract->ite / 4) ? i * 255 / fract->ite : 0));
			coords.y++;
		}
		coords.x++;
	}
}

void	draw_mandel(t_fractdata *fract, int image_x, int image_y, t_env *env)
{
	t_coords	coords;
	int			i;

	coords.x = 0;
	while (coords.x < image_x && coords.x < env->win_size.x)
	{
		fract->c->c_r = coords.x / fract->zoom + fract->x_1;
		coords.y = 0;
		while (coords.y < image_y && coords.y < env->win_size.y)
		{
			fract->c->c_i = coords.y / fract->zoom + fract->y_1;
			fract->c->z_r = 0;
			fract->c->z_i = 0;
			i = env->tab_is_in_fract[env->nb_fract](fract);
			if (i == fract->ite)
				image_put_pixel(*env, coords.x, coords.y, 0);
			else
				image_put_pixel(*env, coords.x, coords.y,
				create_color(0, i * 255 / fract->ite, 0,
				(i <= fract->ite / 4) ? i * 255 / fract->ite : 0));
			coords.y++;
		}
		coords.x++;
	}
}
