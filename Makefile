# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/26 18:04:58 by adubois           #+#    #+#              #
#    Updated: 2016/01/22 17:23:51 by tbalu            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol
CC = gcc
CFLAGS = -Wall -Werror -Wextra
CFLAGS += -g -O0
INC_PATH = -I./inc/ -I./libft/ -I./minilibx_macos/
SRC_PATH = ./src/
SRCS = main.c image.c constructor.c draw_fract.c events.c color.c \
fract_algo.c set_data.c modif_points.c is_in_fract.c
FLAGSMLX = -L minilibx_macos/ -lmlx -framework OpenGL -framework AppKit
FLAGSLIBFT = -L libft/ -lft
SRC = $(SRCS:%.c=$(SRC_PATH)%.c)
OBJ = $(SRC:%.c=%.o)

all: mlx lft $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(FLAGSLIBFT) $(FLAGSMLX)

mlx:
	make -C minilibx_macos/

lft:
	make -C libft/

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS) $(INC_PATH)

siclean:
	rm -f $(OBJ)

sifclean: siclean
	rm -f $(NAME)

sire: sifclean $(NAME)

clean:
	rm -f $(OBJ)
	make -C libft/ clean

fclean: clean
	rm -f $(NAME)
	make -C minilibx_macos/ clean
	make -C libft/ fclean

memclean:
	rm -rf *~

re: fclean all
